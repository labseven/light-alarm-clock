print('garbage '..collectgarbage("count")*1024)
collectgarbage("collect")

function localEpoch()
  local epoch = rtctime.get()
  return epoch + timezoneOffset
end

function getDailyTime()
  -- Returns the number of seconds in the day, and the day of the week
  local tm = rtctime.epoch2cal(localEpoch())
  return tm['hour'] * 3600 + tm['min'] * 60 + tm['sec']
end

function mod(num, divisor)
  local remainder = num
  local loops = 0
  while remainder >= divisor do
    remainder = remainder - divisor
    loops = loops + 1
  end

  return remainder, loops
end

function prettyDailyTime(time)
  local mins, hours = mod(time, 3600)
  mins = mins / 60
  return string.format("%02d:%02d", hours, mins)
end

function prettyFormatedTime()
  local tm = rtctime.epoch2cal(localEpoch())
  return string.format("%02d:%02d %02d/%02d/%04d", tm['hour'], tm['min'], tm['day'], tm['mon'], tm['year'])
end

function prettyAlarmPrint(alarm)
  local days = ""
  for _, day in pairs(alarm['wdays']) do
    days = days .. day .. " "
  end

  local time = prettyDailyTime(alarm['time'])
  local fadeInSec, fadeInMins = mod(alarm['fadeInSec'], 60)
  local stayOnSec, stayOnMins = mod(alarm['keepOnSec'], 60)

  return string.format("name: %s days: %s time: %s fadeIn: %d:%02d stayOn: %d:%02d", alarm['name'], days, time, fadeInMins, fadeInSec, stayOnMins, stayOnSec)
end

function getWday()
  return rtctime.epoch2cal(localEpoch())['wday']
end

function convertToSeconds(time)
  -- input table with hour, min, sec
  if time['sec'] == nil then
    time['sec'] = 0
  end
  return time['hour'] * 3600 + time['min'] * 60 + time['sec']
end

function exp(base, pow)
  local val = 1
  for i = 1, pow, 1 do
    val = val * base
  end
  return val
end

function setupLED(led)
  gpio.mode(led, gpio.OUTPUT)
  gpio.write(led, gpio.LOW)
  pwm.setup(led, 1000, 0)
end

function updateLED(led, alarm)
  local curTime = getDailyTime()
  if curTime < alarm['time'] or curTime > alarm['time'] + alarm['fadeInSec'] + alarm['keepOnSec'] then
    -- print(alarm['name'],": OFF", alarm['time'] - curTime, "sec remain")
    pwm.setduty(led, 0)
    pwm.stop(led)
    -- gpio.write(led, gpio.LOW)
  elseif curTime < alarm['time'] + alarm['fadeInSec'] then
    pwm.start(led)
    local fadeInRatio = (curTime - alarm['time']) / alarm['fadeInSec']
    local intensity = alarm['maxBrightness'] * exp(fadeInRatio, 4)
    -- lowest setting is 2
    if intensity < 2 then intensity = 2 end
    -- print(alarm['name'], ":", fadeInRatio, "% LED:", intensity)
    pwm.setduty(led, intensity)
  else
    -- print(alarm['name'], ": ON", alarm['time'] - curTime + alarm['fadeInSec'] + alarm['keepOnSec'], "sec remain")
    pwm.setduty(led, alarm['maxBrightness'])
    -- pwm.stop(led)
    -- gpio.write(led, gpio.HIGH)
  end
end

function contains(list, test)
  for _, item in pairs(list) do
    if item == test then
      return true
    end
  end
  return false
end

function alarmListTick(alarmList, led)
  local curWday = getWday()
  for _, alarm in pairs(alarmList) do
    if contains(alarm['wdays'], curWday) then
      updateLED(led, alarm)
    end
  end
end

function userListTick(usersList)
  for _, user in pairs(usersList) do
    alarmListTick(user['alarmList'], user['led'])
  end
end

function setupLEDs(usersList)
  for _, user in pairs(usersList) do
    print("Setup LED for", user['name'])
    setupLED(user['led'])
  end
end

function blinkError(numBlinks)
  if numBlinks > 0 or numBlinks == -1 then
    if numBlinks > 0 then numBlinks = numBlinks -1 end
    tmr.create():alarm(250, tmr.ALARM_SINGLE, function ()
      pwm.setduty(1, 1023)
      pwm.setduty(2, 1023)
      tmr.create():alarm(250, tmr.ALARM_SINGLE, function ()
        pwm.setduty(1, 0)
        pwm.setduty(2, 0)
        blinkError(numBlinks)
      end)
    end)
  else
    print("done blinking")
  end
end
