print('hello from application.lua')
print("current time:", rtctime.get())

require('alarmlib')
require('httpserver')

print('blinking on boot')
blinkError(8)

timezoneOffset = -7 * 3600

-- alarm1 = {led= 3, time= getDailyTime() + 20, fadeInSec= 15, keepOnSec= 5}
-- alarm2 = {led= 2, time= getDailyTime() + 20, fadeInSec= 15, keepOnSec= 5}

-- Times: 24600: 6:50

usersList = {
  {
    name= 'Adam',
    led= 1, -- D1
    alarmList= {
      {wdays= {1, 2, 3, 4, 5, 6, 7}, time= 24000, fadeInSec= 600, keepOnSec= 600, maxBrightness= 1000, name='A'},
      -- {wdays= {1, 2, 3, 4, 5, 6, 7}, time= getDailyTime() + 20, fadeInSec= 15, keepOnSec= 5, maxBrightness= 1000, name='A test'},
    }
  },
  {
    name= 'David',
    led= 2, -- D2
    alarmList= {
      {wdays= {1, 2, 3, 4, 5, 6, 7}, time= 24000, fadeInSec= 600, keepOnSec= 600, maxBrightness= 1000, name='D'},
      -- {wdays= {1, 2, 3, 4, 5, 6, 7}, time= getDailyTime() + 20, fadeInSec= 15, keepOnSec= 5, maxBrightness= 1000, name='D test'},
    }
  }
}

-- sync the time every hour
tmr.create():alarm(3600000, tmr.ALARM_AUTO, function ()
  print('Syncing time with NTP')
  sntp.sync()
end)

-- update LED status every second
setupLEDs(usersList)
tmr.create():alarm(1000, tmr.ALARM_AUTO, function ()
  userListTick(usersList)
end)
