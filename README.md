# Light Alarm Clock
An alarm clock that lights up the room to gently wake you up. Great for these short winter days comming up.

Supports multiple users / zones, and alarms based on day of the week.

Running on [ESP8266](https://en.wikipedia.org/wiki/ESP8266) with [NodeMCU](nodemcu.readthedocs.io)


### Coming Soon:™
- ~~Add HTTP server to change alarm settings~~ **Done**
- Persist alarm settings between reboot
- Add automatic DST shift
- Cleanup the code
- Add GIFs to show off
