-- Create HTTP Server
srv=net.createServer(net.TCP)
print("http server started")
srv:listen(80,function(conn)
    conn:on("receive", function(client,request)
        local buf = "";
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end

        if path == "/favicon.ico" then
            client:send("HTTP/1.0 404 Not Found\r\nServer: NodeMCU on ESP8266\r\n\r\n")
            return
        end

        print(request);
        print("vars:")
        local params = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=([{},%w]+)&*") do
                params[k] = v
                print(k, v)
            end
        end

        if method == "POST" then
            print("post params:")
            postParams = {}
            _, _, postVars = string.find(request, "\r\n\r\n(.+)")

            if postVars ~= nil then
                for k, v in string.gmatch(postVars, "([^=^&]+)=([^&]+)") do
                    postParams[k] = v
                    print(k, v)
                end
            end
        end


        if path == "/" then
            local alarmBuf = ""
              for k, user in pairs(usersList) do
                alarmBuf = alarmBuf.. "<h4>[".. k .. "] " .. user['name'] ..":</h4><ul>"
                local lastAlarm = 0
                for i, alarm in pairs(user['alarmList']) do
                    alarmBuf = alarmBuf .. "<li><a href=/alarm?user=".. k.. "&alarm=".. i .. ">[".. i .. "]</a> " .. prettyAlarmPrint(alarm) .."</li>"
                    lastAlarm = i
                end
                alarmBuf = alarmBuf .. "<li><a href=/alarm?user=".. k .."&alarm=".. lastAlarm+1 ..">[new]</a>"
                alarmBuf = alarmBuf .. "</ul>"
              end

            local ledBuf = "<ul>"
            for _, user in pairs(usersList) do
                ledBuf = ledBuf .. "<li>" .. user['name'] .. ": " ..pwm.getduty(user['led']) .. "</li>"
            end
            ledBuf = ledBuf .. "</ul>"

            if file.open("home.html") then
                buf = string.format(file.read(), prettyFormatedTime(), alarmBuf, ledBuf)
                file.close()
            end

            client:send("HTTP/1.0 200 OK\r\nServer: NodeMCU on ESP8266\r\nContent-Type: text/html\r\n\r\n")
            client:send(buf)
        elseif path == "/alarm" and method == "GET" then
            print("GET /alarm")
            local user = usersList[tonumber(params.user)]
            if user ~= nil then
                local alarm = user['alarmList'][tonumber(params.alarm)]
                if alarm == nil then
                    alarm = {wdays= {1, 2, 3, 4, 5, 6, 7}, time= 24000, fadeInSec= 600, keepOnSec= 600, maxBrightness= 1000, name='A'}
                    user['alarmList'][tonumber(params.alarm)] = alarm
                end

                local wdays = ""
                for _, v in pairs(alarm['wdays']) do
                    wdays = wdays .. v .. " "
                end

                local fadeInSec, fadeInMins = mod(alarm['fadeInSec'], 60)
                local stayOnSec, stayOnMins = mod(alarm['keepOnSec'], 60)

                fadeInMins = string.format("%d:%02d", fadeInMins, fadeInSec)
                stayOnMins = string.format("%d:%02d", stayOnMins, stayOnSec)

                print(alarm['fadeInSec'], alarm['keepOnSec'])
                print(alarm['name'], user['name'], prettyDailyTime(alarm['time']), wdays, fadeInMins, stayOnMins, alarm['maxBrightness'], alarm['name'])
                if file.open("editalarm.html") then
                    buf = string.format(file.read(), alarm['name'], user['name'], prettyDailyTime(alarm['time']), wdays, fadeInMins, stayOnMins, alarm['maxBrightness'], alarm['name'])
                    file.close()
                end
            end

            client:send("HTTP/1.0 200 OK\r\nServer: NodeMCU on ESP8266\r\nContent-Type: text/html\r\n\r\n")
            client:send(buf)

        elseif path == "/alarm" and method == "POST" then
            print("POST /alarm")
            print(params.user, params.alarm)

            local user = usersList[tonumber(params.user)]
            if user ~= nil then
                print("editing user")
                if postParams.delete == 'on' then
                    print("deleting alarm!")
                    user['alarmList'][tonumber(params.alarm)] = nil
                end
                local alarm = user['alarmList'][tonumber(params.alarm)]
                if alarm ~= nil then
                    print("editing alarm")
                    if postParams.wdays ~= nil then
                        alarm.wdays = {}
                        for i in string.gmatch(postParams.wdays, "(%d)") do
                            table.insert(alarm.wdays, i)
                        end
                    end
                    print("alarm time", postParams['alarmTime'], postParams.alarmTime)
                    if postParams['alarmTime'] ~= nil then
                        local _, _, newHours, newMins = string.find(postParams['alarmTime'], "(%d+)%%3A(%d+)")
                        print('new time:', newHours, newMins, postParams['alarmTime'])
                        alarm.time = newHours * 3600 + newMins * 60
                    end
                    if postParams.fadeInMins ~= nil then
                        local _, _, newFadeInMins, newFadeInSecs = string.find(postParams.fadeInMins, "(%d+)%%3A(%d+)")
                        alarm.fadeInSec = newFadeInMins * 60 + newFadeInSecs
                    end
                    if postParams.keepOnMins ~= nil then
                        local _, _, newKeepOnMins, newKeepOnSecs = string.find(postParams.keepOnMins, "(%d+)%%3A(%d+)")
                        alarm.keepOnSec = newKeepOnMins * 60 + newKeepOnSecs
                    end
                    if postParams.maxBrightness ~= nil then
                        local newMaxBrightness = tonumber(postParams.maxBrightness)
                        if newMaxBrightness > 1023 then newMaxBrightness = 1023 end
                        if newMaxBrightness < 0 then newMaxBrightness = 0 end
                        alarm.maxBrightness = newMaxBrightness
                    end

                    print(alarm['time'], alarm['fadeInSec'], alarm['keepOnSec'], alarm['maxBrightness'])
                end
            end

            client:send("HTTP/1.0 302 Found\r\nLocation: /\r\nServer: NodeMCU on ESP8266\r\nContent-Type: text/html\r\n\r\n")
        end


        method, path, vars, postVars, params, postParams, request, buf = nil
        collectgarbage("collect")
    end)
    conn:on("sent", function(c) c:close() end)
end)
